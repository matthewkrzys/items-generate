package com.areso.jwzp.items.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Item {
    private final int id;
    private final String name;
    private final BigDecimal price;
}
