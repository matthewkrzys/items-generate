package com.areso.jwzp.items.controllers;

import com.areso.jwzp.items.model.Item;
import com.areso.jwzp.items.services.ItemsService;
import lombok.extern.slf4j.Slf4j;
import org.simpleflatmapper.csv.CsvWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

@Controller
@Slf4j
public class ItemsController {

    private final ItemsService itemsService;

    @Autowired
    public ItemsController(ItemsService itemsService) {
        this.itemsService = itemsService;
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET, produces = {"application/json", "application/xml"})
    @ResponseBody
    public List<Item> getItems() {
        return itemsService.getItems();
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET, produces = "text/csv")
    @ResponseBody
    public String getCsvItems() {
        StringBuffer result = new StringBuffer();
        try {
            CsvWriter<Item> writter = CsvWriter.from(Item.class).to(result);
            for (Item item : itemsService.getItems()) {
                writter.append(item);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return result.toString();
    }
}
