package com.areso.jwzp.items.services;

import com.areso.jwzp.items.model.Item;
import io.vavr.control.Try;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.simpleflatmapper.csv.CsvParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.areso.jwzp.items.util.NullReader.nullReader;

@Component
@Slf4j
public class ItemsService {

    private static final String FILE_NAME = "/items.csv";

    @Getter
    private final List<Item> items;

    @Autowired
    public ItemsService() {
        Reader reader = Try
                .of(() -> (Reader) new InputStreamReader(getClass().getResourceAsStream(FILE_NAME)))
                .onFailure(ex -> log.error("Cannot read file" + FILE_NAME, ex))
                .getOrElse(nullReader());
        items = Try
                .of(() -> CsvParser.mapTo(Item.class).stream(reader))
                .onFailure(ex -> log.error("Error while processing file " + FILE_NAME, ex))
                .getOrElse(Stream.empty())
                .collect(Collectors.toList());
    }
}
