package com.areso.jwzp.items;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemsApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void willFail() {
		Assert.assertTrue(false);
	}
}
